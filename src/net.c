/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tls.h>

#include <err.h>

#include "net.h"
#include "parse/gmi.h"
#include "parse/http.h"

char* net_get(void) {
  struct tls *ctx;
  struct tls_config *config;
  ssize_t ret = 0, bytesRead = 0, buf_size = 100000000; // 100000000 Byte = 100MB
  char* buf = malloc(buf_size);

  gemi_req req = gemi_get("gemini://gemini.circumlunar.space/docs/spec-spec.txt\n");
  gemi_print_req(req);
  // http_req req = http_get("example.com");

  /* https://man.openbsd.org/tls_client.3 */
  if ((ctx = tls_client()) == NULL)
    errx(1, "tls_client: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_config_new.3 */
  if ((config = tls_config_new()) == NULL)
    errx(1, "tls_config_new: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_configure.3 */
  if (tls_configure(ctx, config) == -1)
    errx(1, "tls_configure: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_connect.3 */
  if (tls_connect(ctx, req.server, req.port) == -1)
    errx(1, "tls_connect: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_write.3 */
  if ((ret = tls_write(ctx, req.request, strlen(req.request))) == -1)
    errx(1, "tls_write: %s", tls_error(ctx));
  printf("[-%zd] GEMINI(%s)\n", ret, req.request);

  while(bytesRead < buf_size) {
    /* https://man.openbsd.org/tls_read.3 */
    ret = tls_read(ctx, buf+bytesRead, buf_size-bytesRead);
    printf("[+%zd]\n", ret);

    // TODO: Break when HTTP sends end sequence
    if (ret == 0)
      break;
    if (ret == -1)
      errx(1, "tls_read: %s", tls_error(ctx));

    bytesRead += ret;
  }

  /* https://man.openbsd.org/tls_close.3 */
  if (tls_close(ctx) == -1)
    errx(1, "tls_close: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_free.3 */
  tls_free(ctx);

  return (buf);
}
