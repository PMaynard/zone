/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#ifndef ZONE_NET_H
#define ZONE_NET_H

char* net_get(void);

#endif
