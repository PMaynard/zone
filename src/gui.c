/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "net.h"
#include "parse/gmi.h"
#include "parse/http.h"

#include "gui.h"

#include "raylib.h"
#define RAYGUI_IMPLEMENTATION
#include "vendor/raygui.h"

int gui_init(void)
{

    // Initialization
    //--------------------------------------------------------------------------------------
    gemi_res g_res;
    const int screenWidth = 800;
    const int screenHeight = 450;

    InitWindow(screenWidth, screenHeight, "Zone | Gemini Browser");

    int scrollPositionY = 0;
    // int scrollPositionY = screenHeight/2 - 40;
    int scrollSpeed = 100;            // Scrolling speed in pixels

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second

    Font font = GetFontDefault();       // Get default system font

    Rectangle panelRec = { 0, 0, screenWidth, screenHeight };
    Rectangle panelContentRec = {0, 0, 760, 20000 };
    Vector2 panelScroll = { 99, -20 };

    // Set the size of the scrollbars
    GuiSetStyle(LISTVIEW, SCROLLBAR_WIDTH, 20);
    GuiSetStyle(DEFAULT, BORDER_WIDTH, 0);

    // Boolean options
    bool display_menu = false;
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------

        // ---- Page view
        scrollPositionY -= (GetMouseWheelMove()*scrollSpeed);
        if (scrollPositionY < 0) scrollPositionY = 0;
        // TODO: Page Down/UP scrolling. 
        // if (IsKeyDown(KEY_PAGE_UP)) scrollPositionY -=screenHeight;
        // if (IsKeyDown(KEY_PAGE_DOWN)) scrollPositionY += screenHeight;

        // --- Navivation
        if (display_menu && IsKeyDown(KEY_ENTER)) {
            g_res = gemi_parse_res(net_get());
            display_menu = false;
        }
        
        // --- menu
        if (IsKeyDown(KEY_LEFT_CONTROL) && IsKeyPressed(KEY_TAB)) {
            if (display_menu)
                display_menu = false;
            else
                display_menu =  true;
        }
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
            // ---- Draw Basics
            ClearBackground(BLACK);
            // ---- Draw Basics

            // ---- Draw Debug
            DrawText(FormatText("Y: %03i", scrollPositionY), screenWidth-100, 5, 20, LIGHTGRAY);
            // ---- Draw Debug

            // ---- Draw Page
            DrawText(TextFormat("[%f, %f]", panelScroll.x, panelScroll.y), 4, 4, 20, RED);
            GuiScrollPanel(panelRec, panelContentRec, &panelScroll);

            // if (g_res.url != NULL){
                DrawText(g_res.status, screenWidth-50, 10, 20, RED);
                DrawText(g_res.meta, screenWidth-300, 30, 20, GREEN);
                DrawTextRec(font, g_res.body,
                           (Rectangle){ panelRec.x + panelScroll.x, panelRec.y + panelScroll.y, panelContentRec.width, panelContentRec.height },
                           20.0f, 2.0f, true, GRAY);
            // }

            // ---- Draw Page

            // ---- Draw Menu
            if (display_menu) {
                Create_Display_Menu();
            }

            // ---- Draw Menu
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    // free(text);
    return (EXIT_SUCCESS);
}

void Create_Display_Menu(void){
    DrawText(FormatText("I am a menu, yo!", ""), 100, 5, 20, LIGHTGRAY);
}
