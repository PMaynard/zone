/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#ifndef ZONE_GUI_H
#define ZONE_GUI_H

int gui_init(void); 
void Create_Display_Menu(void);

#endif
