/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

typedef struct Gemini_Request {
	char server[1024];
	char port[1024];
	char request[1024];
} gemi_req;

typedef struct Gemini_Response {
	char url[1024];
	char status[1024];
	char meta[1024];
	char body[1000000]; // 1000000 Byte = 1MB
	// TODO: Figure out why body segfaults at 100MB.
} gemi_res;

void gemi_print_req(gemi_req req);
void gemi_print_res(gemi_res res);

gemi_req gemi_get(char* url);
gemi_res gemi_parse_res(char* res);
