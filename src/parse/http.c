/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#include <string.h>
#include "http.h"

http_req http_get(char* url){
  http_req req;
  strcpy(req.server, "example.com");
  strcpy(req.port, "443");
  strcpy(req.request, "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n");
  return req;
}
