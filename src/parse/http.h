/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

typedef struct HTTP_Request {
	char url[1024];
	char server[1024];
	char port[1024];
	char request[1024];
} http_req;

http_req http_get(char* url);
