/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "gmi.h"

gemi_req gemi_get(char* url){
  gemi_req req;
  strcpy(req.server, "gemini.circumlunar.space");
  strcpy(req.port, "1965");
  // TODO: Make sure URL ends with <CR> 
  strcpy(req.request, url);
  return req;
}

gemi_res gemi_parse_res(char* buff){
	gemi_res res;
	// TODO: Parse depending on status codes, and fix up this hacky hack.
	sscanf(buff, "%s %[A-z;0-9- =/]", res.status, res.meta);
	strcpy(res.url, "TODO: Place the URL here.");
	strcpy(res.body, buff);
	free(buff);
	return res;
} 


void gemi_print_req(gemi_req req){
	printf("GEMINI(%s:%s - [%s])\n", 
		req.server,
		req.port,
		req.request);
}

void gemi_print_res(gemi_res res){
	printf("GEMINI(Status: '%s', Meta: '%s', URL: '%s')\n%s\n", 
		res.status,
		res.meta,
		res.url,
		res.body);
}
