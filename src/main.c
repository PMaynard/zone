/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 May 1st
Description: Zone a simple Gemini browser
License:     BSD-3-Clause
*/

#include <stdio.h>
#include <stdlib.h>

#include <err.h>

#include "gui.h"
#include "net.h"
#include "parse/gmi.h"

#include "cmd.h"

int main(int argc, char **argv) {
	
	if (gui_init() == -1)
		errx(1, "gui_init: -1");

	// cmd();

	return (EXIT_SUCCESS);
}
