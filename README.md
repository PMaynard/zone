# Zone

Zone is a [Gemini](https://gemini.circumlunar.space/) request generator and response parser.

![zone wip](screenshot.png)

## Build 

Dependencies are: 

- [libtls](https://man.openbsd.org/tls_init.3)
- [raylib](https://www.raylib.com/)
	- [glfw](https://www.glfw.org/)
	- [libm](https://en.wikipedia.org/wiki/C_mathematical_functions#libm)
- [Optional] [readline](https://tiswww.case.edu/php/chet/readline/rltop.html) 

To get raylib to work under Fedora 32 with Wayland: 

	make PLATFORM=PLATFORM_DESKTOP USE_WAYLAND_DISPLAY=TRUE USE_EXTERNAL_GLFW=TRUE

To build zone: 

	PKG_CONFIG_PATH=/usr/local/lib/pkgconfig meson build 
	ninja -C build/ && ./build/zone


# Design

The image below shows how I imagine zone to be. Essentially, Firefox in reader mode with multiple columns of text. 

![prototype design, based on firefox's reading mode](prototype.png)
